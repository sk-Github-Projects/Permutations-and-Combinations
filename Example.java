import java.util.ArrayList;

import SkrewEverything.*;

public class Example
{
	public static void main(String[] args)
	{
		Permutations p = new Permutations();
		ArrayList<String> a = p.generate("1 2 3 10", 4, 4); // returns ArrayList with String type
		for(String s : a) 
		{
			System.out.println(s); // prints with space in-between characters
		}
		for(String s : a)
		{
			System.out.println(s.replaceAll(" ", "")); // prints without space in-between characters
		}
	}
}
