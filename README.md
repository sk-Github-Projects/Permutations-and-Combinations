# Permutations

Permutaions is a Java based code to generate all the possible permutations of a given string.

  - You can specify minimum length
  - You can specify maximum length
  - Magic

### Installation

Add the **Permutations.jar** file to your project and import the package

```java
import SkrewEverything.Permutations;
````
Otherwise, copy **SkrewEverything** folder to your source folder and import it.


License
----

MIT


**Free Software, Hell Yeah!**
