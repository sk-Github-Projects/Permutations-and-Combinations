package SkrewEverything;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 * This class provides all the possible permutations of a given string.
 * 
 * @author SkrewEverything
 * @see <a href="https://github.com/SkrewEverything">GitHub</a>
 * 
 */
 
public class Permutations
{

	private BigInteger counter,radix;
	
	/**
	 * 
	 * @param givenString : Pass the string containing required characters separated by space
	 * @param startLength : Starting length of the permutations
	 * @param length      : Ending length of the permutations
	 * @return All the generated strings as ArrayList<String>
	 */
	 
	public ArrayList<String> generate(String givenString,int startLength, int length)
	{
		ArrayList<String> resultList = new ArrayList<String>();
		String main = "0 " + givenString;
		String result,str;
		String[] main1 = main.split(" "),str1;
		radix = new BigInteger(String.valueOf(main1.length));
		convertToDecimal(startLength);
		while(true)
		{
			result = "";
			str = number();
			str1 = str.split(" ");
			if(str1.length > length)
			{
				break;
			}
			else
			{
				for(int i = str1.length-1 ; i>=0; i--)
				{
					result += main1[Integer.parseInt(str1[i])] + " ";
				}
				result = result.trim();
				resultList.add(result);
				counter = counter.add(BigInteger.ONE);
			}
		}
		return resultList;
	}
	private String number()
	{
		StringBuilder str = new StringBuilder();
		BigInteger rem,q=counter;
		while(true)
		{
			rem = q.mod(radix);
			q = q.divide(radix);
			if(q.compareTo(BigInteger.ZERO) == 0)
			 {
				 str.append(String.valueOf(rem)+" ");
				 break;
			 }
			if(rem.compareTo(BigInteger.ZERO) == 0)
			{
				str.setLength(0);
				counter = counter.add(BigInteger.ONE);
				q = counter;
			}
			else
			{
				str.append(String.valueOf(rem)+ " ");
			}
		}
		return str.toString().trim();
	}
	private void convertToDecimal(int l)
	{
		
		BigInteger b = new BigInteger(String.valueOf(radix));
		BigInteger b1 = new BigInteger("0");
		int i = 0;
		for( i = 0; i<l;i++)
		{
			b1 = b1.add(b.pow(i));
		}
		counter = b1;
		
	}
}
